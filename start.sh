#!/usr/bin/env bash
set -e
env
echo $JIRA_SOFTWARE_PORT_8080_TCP_ADDR
sed -i "s/jira-software/$JIRA_SOFTWARE_PORT_8080_TCP_ADDR/g" /etc/nginx/sites-enabled/default

# Start nginx (in the foreground)
nginx -g "daemon off;"

