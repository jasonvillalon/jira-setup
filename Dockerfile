FROM phusion/baseimage:latest
MAINTAINER CodehoodPH <admin@codehood.ph>

# Install system dependencies (in general)
RUN apt-get update && \
    apt-get install -yq git build-essential python-dev nginx

RUN service nginx start
# Expose the server port
EXPOSE 80

# Add nginx configuration
ADD ./default /etc/nginx/sites-enabled/default
ADD ./www /www
# Setup start script
WORKDIR /srv/
ADD ./start.sh /srv/start.sh
ENTRYPOINT ["/srv/start.sh"]
